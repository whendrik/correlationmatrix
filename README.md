# Compare methods to calculate the correlation matrix for (large) matrix

For matrix `A`,  we have

```
np.corrcoef(A) == df.DataFrame(A.T).corr()
```

Hence transpose the matrix, or use `rowvar=1` for `corrcoef()`

## COV matrix and correlation matrix are... correlated


`cov2corr()` & `.cov2corr()` within statsmodels helpers can transform back and forth.

Possible (not tested) relevant discussion:

https://stats.stackexchange.com/questions/360786/estimating-correlation-matrix-using-numeric-likelihood-maximization

```
from statsmodels.stats import moment_helpers
moment_helpers.cov2corr(cov.covariance_)
```
